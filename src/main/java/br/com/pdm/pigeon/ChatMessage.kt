package br.com.pdm.pigeon

import java.text.SimpleDateFormat
import java.util.Date
import java.util.logging.SimpleFormatter

class ChatMessage(val text: String, val senderId:Int, val timestamp: Long = Date().time) {
    val moment: String
    get() = SimpleDateFormat("HH:mm").format(timestamp)

}