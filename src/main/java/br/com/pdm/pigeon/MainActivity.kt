package br.com.pdm.pigeon

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.text.SimpleDateFormat
import java.util.Date

const val USER_ID = 0
const val OTHER_ID = 1
class MainActivity : AppCompatActivity() {
    private var fromUser = true
    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val messageList = findViewById<RecyclerView>(R.id.message_list)
        setUpListeners(messageList)
        setUpRecyclerView(messageList)
    }
    private fun setUpListeners(messageList: RecyclerView){

        val mesDig = findViewById<EditText>(R.id.message_edittext)
        val btn = findViewById<Button>(R.id.send_button)
        btn.setOnClickListener{
            val messageText = mesDig.text.toString()
            mesDig.setText("")

            val adapter = messageList.adapter
            if(adapter is MessageAdapter){
                val message = ChatMessage(messageText,if(fromUser) USER_ID else OTHER_ID)
                adapter.addItem(message)
                messageList.scrollToPosition(adapter.itemCount-1)
                fromUser = !fromUser
            }

        }
    }
    private fun setUpRecyclerView(messageList:RecyclerView){
        messageList.layoutManager = LinearLayoutManager(this)
        messageList.adapter = MessageAdapter()
    }
}
